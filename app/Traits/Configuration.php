<?php

namespace App\Traits;

use App\Models\Setting;

trait Configuration
{
    public function getSunApiUrl()
    {
        return Setting::where('name', 'sun_url')->value('value');
    }

    public function getSunSecret()
    {
        return Setting::where('name', 'sun_secret')->value('value');
    }

    public function getSunKey()
    {
        return Setting::where('name', 'sun_key')->value('value');
    }

    public function getShopifyUrl()
    {
        return Setting::where('name', 'shopify')->value('value');
    }
}
