<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Image extends Model
{
    /*all product belong to specific shop*/
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
