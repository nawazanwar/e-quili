<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    protected $fillable = ['name', 'access_token','hmac','auth_token'];

    /* one shop has many products*/
    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
