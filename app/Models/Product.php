<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = [
        'is_updated',
        'shop_id',
        'product_id',
        'sku',
        'name',
        'estimated_market_price',
        'description',
        'image',
        'category_id',
        'ean',
        'product_model',
        'brand',
        'take_bid_minimum_price',
        'alert_minimum_price',
        'counter_offer_minimum_price',
        'counter_offer_maximum_price',
        'counter_offer_price',
        'active_equili',
        'equili_limit',
        'equili_from',
        'equili_to',
        'short_description',
    ];

    protected $dates = ['created_at', 'updated_at'];


    public function getCategory()
    {
        if ($this->category()->count() > 0) {
            return $this->category()->value('name');
        } else {
            return "No Category";
        }
    }

    /*all products belong to specific shop*/
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /* one product has many images*/
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    //one product belongs to many categories
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function getLastSku($shop_id)
    {
        if (self::where('shop_id', $shop_id)->count() > 0) {
            return self::where('shop_id', $shop_id)->orderBy('id', 'desc')->first()->product_id;
        } else {
            return 0;
        }
    }
}
