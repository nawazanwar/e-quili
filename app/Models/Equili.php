<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;

class Equili extends Model
{

    public static function check_product_status($product_id)
    {
        $url = "https://e-quili.com/api/products/check-status/" . $product_id;
        return json_decode(file_get_contents($url), true);
    }

    public static function getCategories()
    {
        return json_decode(file_get_contents("https://e-quili.com/api/categories/menu"), true);
    }

    public static function login($params)
    {
        return self::call("https://e-quili.com/api/auth/login", 'POST', $params);
    }

    public static function call($url, $method = 'GET', $params = null)
    {
        if ($method == 'POST' || $method = 'post') {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            return json_decode($response, true);
            curl_close($crl);
        }
    }

    public static function update_product($token, $queryParams)
    {
        header('Content-Type: application/json'); // Specify the type of data
        $ch = curl_init("https://e-quili.com/api/supplier/products/plugin-update"); // Initialise cURL
        $postParams = json_encode($queryParams); // Encode the data array into a JSON string
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token)); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement
        curl_close($ch); // Close the cURL connection
        return json_decode($result, true);
    }

    public static function sync($token, $queryParams)
    {
        header('Content-Type: application/json'); // Specify the type of data
        $ch = curl_init("https://e-quili.com/api/supplier/products/create/bulk"); // Initialise cURL
        $postParams = json_encode($queryParams); // Encode the data array into a JSON string
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token)); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement
        curl_close($ch); // Close the cURL connection
        return json_decode($result, true);
    }

    public static function remove_product($token, $queryParams)
    {
        header('Content-Type: application/json'); // Specify the type of data
        $ch = curl_init("https://e-quili.com/api/supplier/products/remove"); // Initialise cURL
        $postParams = json_encode($queryParams); // Encode the data array into a JSON string
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token)); // Inject the token into the header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams); // Set the posted fields
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $result = curl_exec($ch); // Execute the cURL statement
        curl_close($ch); // Close the cURL connection
        return json_decode($result, true);
    }
}
