<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'value', 'parent'];

    public static function childs($parent)
    {
        return self::where('parent', $parent)->get();
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
