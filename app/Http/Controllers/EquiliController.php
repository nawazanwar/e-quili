<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Equili;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;

class EquiliController extends Controller
{
    public function login(Request $request, $pId)
    {
        $postParams = array(
            'email' => $request->email,
            'password' => $request->password,
            'remember' => 0
        );

        $response = Equili::login($postParams);
        if (isset($response['validation_errors'])) {
            return response()->json(['status' => 'validate_errors', 'errors' => $response['validation_errors']]);
        } else if (isset($response['error'])) {
            return response()->json(['status' => 'error', 'message' => $response['error']['description']]);
        } else {
            $shop = Shop::find($pId);
            $seller_token = $response['access_token'];
            $seller_id = isset($response['user_info']['id']) ? $response['user_info']['id'] : null;
            $first_name = isset($response['user_info']['first_name']) ? $response['user_info']['first_name'] : '';
            $last_name = isset($response['user_info']['last_name']) ? $response['user_info']['last_name'] : '';

            $seller_name = $first_name . " " . $last_name;
            $shop->seller_token = $seller_token;
            $shop->seller_name = $seller_name;
            $shop->seller_id = $seller_id;

            if ($shop->save()) {
                return response()->json(['status' => 'true', 'response' => $response]);
            }
        }
    }

    public function logout(Request $request, $shop_id)
    {
        $shop = Shop::find($shop_id);
        $ids = Product::where('shop_id', $shop->id)->whereNotNull('equili_id')->pluck('equili_id')->toArray();
        $data = ['product_ids' => $ids];
        Equili::remove_product("Authorization: Bearer " . $shop->seller_token, $data);
        foreach ($ids as $id) {
            $product = Product::where('equili_id', $id)->first();
            $product->is_deleted_from_store = true;
            $product->save();
        }
        $shop->seller_token = null;
        $shop->seller_name = null;
        if ($shop->save()) {
            $deleted_rows = array();
            foreach ($ids as $id) {
                $product = Product::where('equili_id', $id)->first();
                $deleted_rows['updated_row_' . $product->id] = view('products.td', ['product' => $product])->render();
            }
            return response()->json(['status' => 'true', 'deleted_rows' => $deleted_rows]);
        }
    }

    public function manage(Request $request)
    {
        if ($request->ajax()) {
            $categories = Category::whereParent(0)
                ->orderby('name', 'asc')
                ->get();
            return view('equili.scan.index', array('categories' => $categories))->render();
        }
    }

    public function check_product_status()
    {
        $products = Product::where('equili_status', 'Pending')->select('equili_id', 'id')->get();
        if (count($products) > 0) {
            foreach ($products as $product) {
                $response = Equili::check_product_status($product->equili_id);
                if (isset($response['status'])) {
                    $product->equili_status = $response['status'];
                    $product->save();
                }
            }
        }
    }

    public function delete_product($eqId)
    {
        $mode_id = Product::where('equili_id', $eqId)->value('id');
        $product = Product::find($mode_id);
        $shop = Shop::find($product->shop_id);
        $data = ['product_ids' => array($eqId), "isWordpress" => true];
        $response = Equili::remove_product("Authorization: Bearer " . $shop->seller_token, $data);;
        if (isset($response['success']) && $response['success'] == '1') {
            $product->is_deleted_from_store = true;
            if ($product->save()) {
                return response()->json([
                    'status' => 'true',
                    'response' => $response,
                    'message' => $product->name . " has been deleted from my store",
                    'product' => $product,
                    'updated_row' => view('products.td', ['product' => $product])->render()
                ]);
            }
        }
    }

    public function update_product_status($sId)
    {

        $products = Product::where('equili_status', 'Pending')
            ->where('shop_id', $sId)
            ->select('equili_id', 'id', 'product_id', 'shop_id')
            ->get();
        if (count($products) > 0) {
            $approved_products = array();
            foreach ($products as $product) {
                $response = Equili::check_product_status($product->equili_id);
                if (isset($response['status'])) {
                    $status = $response['status'];
                    $approved_products[$product->id] = $status;
                    $product->equili_status = $status;
                    $product->save();
                }
            }
            if (count($approved_products) > 0) {
                return response()->json([
                    'status' => 'true',
                    'products' => $approved_products
                ]);
            }
        }
    }
}

