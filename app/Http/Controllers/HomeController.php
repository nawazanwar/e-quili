<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Equili;
use App\Models\Image;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Setting;
use App\Models\Shopify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $shop = $request->query('shop', null);
        $hmac = $request->query('hmac', null);
        $timestamp = $request->query('timestamp', null);
        if (Shop::where('name', $shop)->exists()) {
            //Check if App is already deleted by Hooks
            $token = Shop::where('name', $shop)->value('access_token');
            $hooks = json_decode(Shopify::call($token, $shop, '/admin/api/' . config('system.api_version') . '/webhooks.json', array("topic" => 'app/uninstalled'), 'GET')['response'], true);
            if (isset($hooks['errors']) && $hooks['errors'] == '[API] Invalid API key or access token (unrecognized login or wrong password)') {
                return redirect()->route('app.install', ['hmac' => $hmac, 'shop' => $shop, 'timestamp', $timestamp]);
            } else {
                //check request came from app or install url
                if ($request->has('session')) {
                    /*url come form app*/
                    $shopModel = Shop::where('name', $shop)->first();
                    if ($shopModel->seller_token != '') {
                        $shopModel = Shop::where('name', $shop)->first();
                        return view('dashboard', ['shop' => $shopModel]);
                    } else {
                        $shopModel = Shop::where('name', $shop)->first();
                        return view('equili.login', ['shop' => $shopModel]);
                    }
                } else {
                    /*url come form installation*/
                    $return_url = 'https://' . $shop . "/admin/apps/e-quili";
                    header("Location: " . $return_url);
                    die();
                }
            }
        } else {
            return redirect()->route('app.install', ['hmac' => $hmac, 'shop' => $shop, 'timestamp', $timestamp]);
        }
    }

    public function install(Request $request)
    {
        $shop = $request->query('shop', null);
        Shop::updateOrCreate(array('name' => $shop), array('name' => $shop, 'hmac' => $request->query('hmac')));
        $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . config('system.api_key') . "&scope=" . trim(config('system.scopes')) . "&redirect_uri=" . urlencode(config('system.redirect_uri'));
        header("Location: " . $install_url);
        die();
    }

    public function generate_token(Request $request)
    {
        $params = $_GET;
        $hmac = $_GET['hmac'];
        $params = array_diff_key($params, array('hmac' => ''));
        ksort($params);
        $computed_hmac = hash_hmac('sha256', http_build_query($params), config('system.secret_key'));
        if (hash_equals($hmac, $computed_hmac)) {
            $query = array(
                "client_id" => config('system.api_key'),
                "client_secret" => config('system.secret_key'),
                "code" => $params['code']
            );
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);
            $access_token = $result['access_token'];
            // update the access token
            $shop = $request->query('shop', null);
            $config = Shop::where('name', $shop)->first();
            $config->access_token = $access_token;
            $config->save();

            $themes = Shopify::call($access_token, $params['shop'], "/admin/api/" . config('system.api_version') . "/themes.json", array(), 'GET')['response'];
            $themes = json_decode($themes, true)['themes'];
            $active_theme_id = '';
            foreach ($themes as $theme) {
                $active_theme_id = ($theme['role'] == 'main') ? $theme['id'] : '';
            }
            $config->active_theme_id = $active_theme_id;
            $config->save();
            /*Start Categories*/
            $categories = Equili::getCategories();
            if (count($categories)) {
                foreach ($categories as $category) {
                    /*create or update the parent categories*/
                    Category::updateOrCreate(
                        ['name' => $category['name'], 'value' => $category['id']],
                        ['name' => $category['name'], 'parent' => 0, 'value' => $category['id']]
                    );
                    if (isset($category['children']) && count($category['children']) > 0) {
                        foreach ($category['children'] as $sub_category) {
                            Category::updateOrCreate(
                                ['name' => $sub_category['name'], 'value' => $sub_category['id']],
                                ['name' => $sub_category['name'], 'parent' => $sub_category['parent_id'], 'value' => $sub_category['id']]
                            );
                        }
                    }
                }
            }

            /*start fetching Products*/
            $this->syncProducts($config->id);
            /*end Fetching Products*/
        } else {
            die('This request is NOT from Shopify!');
        }
    }

    public function syncProducts($shop_id)
    {
        $shop = Shop::find($shop_id);
        $params = array('limit' => 50);
        $end_point = "/admin/api/" . config('system.api_version') . "/products.json";
        $products = Shopify::call($shop->access_token, $shop->name, $end_point, $params, 'GET');
        $products = json_decode($products['response'], JSON_PRETTY_PRINT)['products'];
        $counter = 0;
        foreach ($products as $product) {
            $product_id = strval($product['id']);
            Product::updateOrCreate(
                [
                    'product_id' => $product_id,
                    'shop_id' => $shop_id
                ], [
                'product_id' => $product_id,
                'shop_id' => $shop_id,
                'name' => $product['title'],
                'description' => strip_tags($product['body_html'])
            ]);
            $product_id = (int)$product_id;
            /*get the product default image and all images*/
            $images = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/" . $product_id . "/images.json", array(), 'GET');
            $images = json_decode($images['response'], JSON_PRETTY_PRINT);
            $default_image = isset($images['images'][0]['src']) ? $images['images'][0]['src'] : null;

            /*get the product default price*/

            $variants = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/" . $product_id . "/variants.json", array(), 'GET');
            $variants = json_decode($variants['response'], JSON_PRETTY_PRINT);
            $default_price = isset($variants['variants'][0]['price']) ? $variants['variants'][0]['price'] : null;
            $default_sku = isset($variants['variants'][0]['sku']) ? $variants['variants'][0]['sku'] : null;

            $product_model = Product::where('product_id', strval($product_id))->first();
            //save all images
            if (isset($images['images']) && count($images['images']) > 0) {
                foreach ($images['images'] as $image) {
                    $image_model = new Image();
                    $image_model->src = $image['src'];
                    $product_model->images()->save($image_model);
                }
            }
            //now save default image and default price
            $product_model->image = $default_image;
            $product_model->estimated_market_price = $default_price;
            $product_model->sku = $default_sku;
            $product_model->save();
            // then it is last iteration
            if ($counter == count($products) - 1) {
                $return_url = 'https://' . $shop->name . "/admin/apps/e-quili";
                header("Location: " . $return_url);
                die();
            }
            $counter = $counter + 1;
        }
    }
}

