<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Shopify;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function scan_products(Request $request)
    {
        $shop_id = $request->get('shop_id', null);
        $since_id = $request->get('since_id', null);
        $shop = Shop::find($shop_id);
        $params = array('limit' => 150);
        if ($since_id > 0) {
            $params['since_id'] = $since_id;
        }
        $end_point = "/admin/api/" . config('system.api_version') . "/products.json";
        $products = Shopify::call($shop->access_token, $shop->name, $end_point, $params, 'GET');
        $products = json_decode($products['response'], JSON_PRETTY_PRINT)['products'];
        $counter = 0;
        foreach ($products as $product) {
            $product_id = strval($product['id']);
            Product::updateOrCreate(
                [
                    'product_id' => $product_id,
                    'shop_id' => $shop_id
                ],[
                    'product_id' => $product_id,
                    'shop_id' => $shop_id,
                    'name' => $product['title'],
                    'description' => strip_tags($product['body_html'])
                ]);
            $product_id = (int)$product_id;
            /*get the product default image and all images*/
            $images = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/" . $product_id . "/images.json", array(), 'GET');
            $images = json_decode($images['response'], JSON_PRETTY_PRINT);
            $default_image = isset($images['images'][0]['src']) ? $images['images'][0]['src'] : null;

            /*get the product default price*/

            $variants = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/" . $product_id . "/variants.json", array(), 'GET');
            $variants = json_decode($variants['response'], JSON_PRETTY_PRINT);
            $default_price = isset($variants['variants'][0]['price']) ? $variants['variants'][0]['price'] : null;
            $default_sku = isset($variants['variants'][0]['sku']) ? $variants['variants'][0]['sku'] : null;

            $product_model = Product::where('product_id', strval($product_id))->first();
            //save all images
            if (isset($images['images']) && count($images['images']) > 0) {
                foreach ($images['images'] as $image) {
                    $image_model = new Image();
                    $image_model->src = $image['src'];
                    $product_model->images()->save($image_model);
                }
            }
            //now save default image and default price
            $product_model->image = $default_image;
            $product_model->estimated_market_price = $default_price;
            $product_model->sku = $default_sku;
            $product_model->save();

            if ($counter == count($products) - 1) {
                $total_products = json_decode(Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/count.json", array(), 'GET')['response'], true)['count'];
                $scanned_products = Product::where('shop_id', $shop_id)->count();
                $scanned_percentage = round(($scanned_products / $total_products) * 100, 2);
                $last_product = Product::getLastSku($shop_id);
                return response()->json([
                    'status' => 'true',
                    'total_products' => $total_products,
                    'scanned_products' => $scanned_products,
                    'scanned_percentage' => $scanned_percentage,
                    'last_product' => $last_product
                ]);
            }

            $counter = $counter + 1;
        }
    }

    public function manage(Request $request)
    {
        if ($request->ajax()) {
            $shop_id = $request->get('shop_id');
            $shop = Shop::find($shop_id);
            $total_products = intval(json_decode(Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/count.json", array(), 'GET')['response'], true)['count']);
            $scanned_products = intval(Product::where('shop_id', $shop_id)->count());
            $scanned_percentage = round(($scanned_products / $total_products) * 100, 2);
            $last_product = Product::getLastSku($shop_id);
            $store_name = 'https://' . $shop->name;
            return view('shop.scanning.manage', compact('scanned_percentage', 'last_product', 'store_name'));
        }
    }

}
