<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Models\Shopify;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function settings(Request $request)
    {
        if ($request->ajax()) {
            return view('settings.index')->render();
        }
    }

    public function add_snippet(Request $request)
    {
        if ($request->ajax()) {
            $src = $request->input('src', null);
            $shop = Shop::find($request->input('shop_id', null));
            $image_url = asset('img/' . $src);
            $snippetValue = <<<SNIPPETDOCS
<div class='equili_btn_holder' style='display:none;'>
    <a class="equili_btn">
      <input type="image" title="$src" src="$image_url" class="form-control p-0" style="border:none;">
    </a>
</div>
<link rel="stylesheet" href="https://logiceverest.com/equili/public/plugins/colorbox/colorbox.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://logiceverest.com/equili/public/plugins/colorbox/jquery.colorbox.js"></script>
<script>
    jQuery.noConflict();
    jQuery(document).ready(function($) {
        $(".equili_btn").colorbox({
            iframe: true,
            fixed: true,
            width: "75%",
            maxWidth: "470px",
            height: "90%"
        });
        var product = {{ product | json }};

        var curreny = {{ product.price | money_with_currency  | json }};
        curreny = curreny.split(" ")[1];
        var localProduct = get_local_product(product.id);

        if (localProduct.equili_id!=''){

          var route = "https://e-quili.com/api/products/check-status/"+localProduct.equili_id;
          console.log("route",route);
          jQuery.getJSON(route, function(result){
             if (result.status=="Approved" && localProduct.active_equili=="1" && localProduct.is_deleted_from_store =="0"){
                 var e_product_route = "https://e-quili.com/bidsPlugin/"+localProduct.equili_id+"?iframe=wordpress&currency="+curreny+"&supplierId="+localProduct.shop.seller_id;
                 $(".equili_btn").removeAttr('href').attr('href',e_product_route);
                if (localProduct.equili_limit=='no_time_limit'){
                     $(".equili_btn_holder").show();
                 }else if (localProduct.equili_limit=='active_on_certain_date'){
                     var current_date = new Date();
                     var to_date = new Date(Date.parse(localProduct.equili_to));
                     if (to_date<current_date){
                         $(".equili_btn_holder").hide();
                     }else{
                         $(".equili_btn_holder").show();
                     }
                 }
             }
          });

           window.setInterval(function() {
                 jQuery.getJSON(route, function(result){
                     if (result.status!='Approved'){
                         $(".equili_btn_holder").hide();
                     }
                 });
           }, 6000);

        }/*end check if equili id is not empty*/
    });
    function get_local_product(pId) {
     var localProduct;
     jQuery.ajax({
            type: 'GET',
            'async': false,
            'global': false,
            url: "https://logiceverest.com/equili/public/product/show/" + pId,
            success: function(response) {
                localProduct = response;
            }
        });
     return localProduct;
    }

</script>
SNIPPETDOCS;

            /*first of all del the snippet*/

            $themes = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/themes.json", array(), 'GET')['response'];
            $themes = json_decode($themes, true)['themes'];
            $active_theme_id = '';
            foreach ($themes as $theme) {
                $active_theme_id = ($theme['role'] == 'main') ? $theme['id'] : '';
            }
            $shop->active_theme_id = $active_theme_id;
            $shop->save();
            $snippet_array = [
                "asset" => [
                    "key" => "snippets/equili.liquid",
                    "value" => $snippetValue
                ]
            ];
            $themes = Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/themes.json", array(), 'GET')['response'];
            $themes = json_decode($themes, true)['themes'];
            $active_theme_id = '';
            foreach ($themes as $theme) {
                $active_theme_id = ($theme['role'] == 'main') ? $theme['id'] : '';
            }
            $shop->active_theme_id = $active_theme_id;
            $shop->save();

            $end_point_for_snippet = "/admin/api/" . config('system.api_version') . "/themes/" . $active_theme_id . "/assets.json";
            $response_snippet = Shopify::call($shop->access_token, $shop->name, $end_point_for_snippet, $snippet_array, 'PUT');
            return response()->json([
                'status' => 'true',
            ]);
        }
    }
}
