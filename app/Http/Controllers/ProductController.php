<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Equili;
use App\Models\Image;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Shopify;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $shop_id = $request->get('shop_id', null);
        $shop = Shop::find($shop_id);
        $products = Product::where('shop_id', $shop_id);
        /* Filter Title*/
        $filter_name = $request->get('filter_name', null);
        if (isset($filter_name) && $filter_name != "undefined") {
            $products = $products->where('name', 'LIKE', '%' . $filter_name . '%');
        }
        $filter_sku = $request->get('filter_sku', null);
        if (isset($filter_sku) && $filter_sku != "undefined") {
            $products = $products->where('sku', 'LIKE', '%' . $filter_sku . '%');
        }
        $filter_equili_status = $request->get('filter_equili_status', null);
        if (isset($filter_equili_status) && $filter_equili_status != "undefined") {
            $products = $products->where('equili_status', 'LIKE', '%' . $filter_equili_status . '%');
        }
        /*filter categories*/
        $filter_category_id = $request->get('filter_category_id', null);
        if (isset($filter_category_id) && $filter_category_id != "undefined") {
            $products = $products->whereHas('category', function ($q) use ($filter_category_id) {
                $q->where('id', $filter_category_id);
            });
        }
        $products = $products->paginate(100);
        if ($request->ajax()) {
            //get the last info
            $total_products = json_decode(Shopify::call($shop->access_token, $shop->name, "/admin/api/" . config('system.api_version') . "/products/count.json", array(), 'GET')['response'], true)['count'];
            $scanned_products = Product::where('shop_id', $shop_id)->count();
            $last_product = Product::getLastSku($shop_id);
            return response()->json([
                'status' => 'true',
                'total_products' => $total_products,
                'scanned_products' => $scanned_products,
                'last_product' => $last_product,
                'next_page' => $products->nextPageUrl(),
                'html' => view('products.single', compact('products'))->render()
            ]);
        }
    }

    public function images(Request $request)
    {
        $images = Image::where('product_id', $request->get('pId'))->get();
        return view('products.images', compact('images'))->render();
    }

    public function edit(Request $request, $id)
    {
        $product = Product::find($id);
        $categories = Category::whereParent(0)
            ->orderby('name', 'asc')
            ->get();
        return view('products.edit', compact('product', 'categories'))->render();
    }

    public function update(Request $request, $eqId)
    {

        $product = Product::where('equili_id', $eqId)->first();
        $product->description = $request->input('description', null);
        $product->take_bid_minimum_price = $request->input('take_bid_minimum_price', null);
        $product->alert_minimum_price = $request->input('alert_minimum_price', null);
        $product->counter_offer_minimum_price = $request->input('counter_offer_minimum_price', null);
        $product->counter_offer_maximum_price = $request->input('counter_offer_maximum_price', null);
        $product->counter_offer_price = $request->input('counter_offer_price', null);
        $send_to_my_store = $request->input('send_to_my_store', 0);

        if ($send_to_my_store) {

            $equili_limit = $request->input('equili_limit', null);
            if ($equili_limit == 'no_time_limit') {
                $product->equili_limit = $equili_limit;
                $product->equili_from = null;
                $product->equili_to = null;
            } else {
                $product->equili_limit = $equili_limit;
                $product->equili_from = $request->input('equili_from', null);
                $product->equili_to = $request->input('equili_to', null);
            }

            $active_equili = $request->input('active_equili', 0);
            if (!$active_equili) {
                $product->active_equili = false;
                $product->equili_limit = null;
                $product->equili_from = null;
                $product->equili_to = null;
            } else {
                $product->active_equili = true;
            }

        }
        $product->short_description = $request->input('short_description', null);
        $product->is_updated = true;
        $product->is_deleted_from_store = 0;
        if ($product->save()) {
            $data = [
                "take_bid_minimum_price_code" => "USD",
                "take_bid_minimum_price" => $product->take_bid_minimum_price,
                "counter_offer_minimum_price_code" => "USD",
                "counter_offer_minimum_price" => $product->counter_offer_minimum_price,
                "counter_offer_maximum_price_code" => "USD",
                "counter_offer_maximum_price" => $product->counter_offer_maximum_price,
                "counter_offer_price_code" => "USD",
                "alert_minimum_price_code" => "USD",
                "alert_minimum_price" => $product->alert_minimum_price,
                "counter_offer_price" => $product->counter_offer_price,
                "id" => $product->equili_id,
            ];
            if ($send_to_my_store) {

                $data["active_equili_time"] = ($product->equili_limit == 'no_time_limit') ? '0' : '1';
                $data["equili_from"] = "{$product->equili_from}";
                $data["equili_to"] = "{$product->equili_to}";
                $queryParams = [
                    'data' => $data
                ];
                $shop = Shop::find($product->shop_id);
                $token = "Authorization: Bearer " . $shop->seller_token;
                $response = Equili::update_product($token, $queryParams);
                if (isset($response['error'])) {
                    return response()->json([
                        'error' => $response['error'],
                        'status' => 'false',
                        'data' => $queryParams
                    ]);
                } else if (isset($response['success']) && $response['success'] == 1) {
                    return response()->json([
                        'data' => $queryParams,
                        'status' => 'true',
                        'product' => $product,
                        'updated_row' => view('products.td', ['product' => $product])->render()
                    ]);
                }
            }

        }
    }

    public function sync(Request $request, $id)
    {
        $product = Product::find($id);
        $product->brand = $request->input('brand', null);
        $product->product_model = $request->input('product_model', null);
        $product->ean = $request->input('ean', null);
        $product->sku = $request->input('sku', null);
        $product->description = $request->input('description', null);
        $product->take_bid_minimum_price = $request->input('take_bid_minimum_price', null);
        $product->alert_minimum_price = $request->input('alert_minimum_price', null);
        $product->counter_offer_minimum_price = $request->input('counter_offer_minimum_price', null);
        $product->counter_offer_maximum_price = $request->input('counter_offer_maximum_price', null);
        $product->counter_offer_price = $request->input('counter_offer_price', null);
        if ($request->has('category_id')) {
            $category_id = $request->input('category_id', null);
            $product->category_id = Category::where("value", $category_id)->value('id');
        }
        $active_equili = $request->input('active_equili', 0);
        if ($active_equili) {
            $equili_limit = $request->input('equili_limit', null);
            if ($equili_limit == 'no_time_limit') {
                $product->equili_limit = $equili_limit;
                $product->equili_from = null;
                $product->equili_to = null;
            } else {
                $product->equili_limit = $equili_limit;
                $product->equili_from = $request->input('equili_from', null);
                $product->equili_to = $request->input('equili_to', null);
            }

        }
        $product->is_updated = false;
        $product->active_equili = $active_equili;
        $product->short_description = $request->input('short_description', null);
        if ($product->save()) {
            $shop = Shop::find($product->shop_id);
            $equili_data = $this->getEquiliData($product);
            $response = Equili::sync("Authorization: Bearer " . $shop->seller_token, $equili_data);
            if (isset($response['products'])) {
                $cProduct = explode(",", $response['products'][$product->product_id]);
                $product->equili_id = $cProduct[0];
                $product->equili_status = $cProduct[1];
                $product->save();
                return response()->json([
                    'equili' => $response,
                    'status' => 'true',
                    'product' => $product,
                    'updated_row' => view('products.td', ['product' => $product])->render()
                ]);
            } else {
                return response()->json([
                    'status' => 'false',
                    'equili' => $response
                ]);
            }
        }
    }

    public function getEquiliData(Product $product)
    {
        $category = Category::find($product->category_id);
        /*initialized data*/
        $queryParams = [
            "data" => [
                $product->product_id => [
                    "is_updated" => $product->is_updated,
                    "equili_status" => "{$product->equili_status}",
                    "id" => "{$product->product_id}",
                    "name" => "{$product->name}",
                    "estimated_market_price" => "{$product->estimated_market_price}",
                    "estimated_market_price_code" => "ILS",
                    "product-match" => "",
                    "category" => "{$category->value}",
                    "take_bid_minimum_price_code" => "ILS",
                    "take_bid_minimum_price" => "{$product->take_bid_minimum_price}",
                    "counter_offer_minimum_price_code" => "ILS",
                    "counter_offer_minimum_price" => "{$product->counter_offer_minimum_price}",
                    "counter_offer_maximum_price_code" => "ILS",
                    "counter_offer_maximum_price" => "{$product->counter_offer_maximum_price}",
                    "counter_offer_price_code" => "ILS",
                    "alert_minimum_price_code" => "ILS",
                    "alert_minimum_price" => "{$product->alert_minimum_price}",
                    "counter_offer_price" => "{$product->counter_offer_price}",
                    "brand" => "{$product->brand}",
                    "product_model" => "{$product->product_model}",
                    "ean" => "{$product->ean}",
                    "sku" => "{$product->sku}",
                    "active_equili" => "{$product->active_equili}",
                    "active_equili_time" => "{($product->equili_limit=='active_on_certain_date')?1:''}",
                    "equili_from" => "{$product->equili_from}",
                    "equili_to" => "{$product->equili_to}",
                    "description" => "{$product->description}",
                    'image' => Image::where('product_id', $product->id)->get()->pluck('src')
                ]
            ]
        ];

        return $queryParams;

    }

    public function show(Request $request, $pId)
    {
        return Product::with('shop')->whereProductId($pId)->first();
    }
}
