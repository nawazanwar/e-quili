<?php

namespace App\Http\Middleware;

use Closure;

class XFrame
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('X-Frame-Options', 'ALLOW FROM https://*.myshopify.com/');
        return $response;
    }
}
