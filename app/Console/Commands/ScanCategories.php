<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Equili;
use Illuminate\Console\Command;

class ScanCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scan:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan equili categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Equili::getCategories();
        if (count($categories)) {
            foreach ($categories as $category) {
                /*create or update the parent categories*/
                Category::updateOrCreate(
                    ['name' => $category['name'], 'value' => $category['id']],
                    ['name' => $category['name'], 'parent' => 0, 'value' => $category['id']]
                );
                if (isset($category['children']) && count($category['children']) > 0) {
                    foreach ($category['children'] as $sub_category) {
                        Category::updateOrCreate(
                            ['name' => $sub_category['name'], 'value' => $sub_category['id']],
                            ['name' => $sub_category['name'], 'parent' => $sub_category['parent_id'], 'value' => $sub_category['id']]
                        );
                    }
                }
            }
        }
    }
}
