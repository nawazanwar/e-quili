<?php

namespace App\Console\Commands;

use App\Models\Equili;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Shopify;
use Illuminate\Console\Command;

class EquiliStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'equili:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where('equili_status', 'Pending')->select('equili_id', 'id', 'product_id', 'shop_id')->get();
        if (count($products) > 0) {
            foreach ($products as $product) {
                $response = Equili::check_product_status($product->equili_id);
                if (isset($response['status'])) {
                    $product->equili_status = $response['status'];
                    $product->save();
                }
            }
        }
    }
}
