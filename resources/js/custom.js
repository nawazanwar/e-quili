var Ajax = new function () {
    this.call = function (url, data, method = 'GET', callback) {
        Ajax.setAjaxHeader();
        $.ajax({
            type: method,
            global: false,
            async: true,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function () {
                console.log("Error Occurred");
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};

var Custom = new function () {
    this.replaceSpaceWithHypen = function (cElement) {
        $(cElement).val($(cElement).val().replace(/\s+/g, '-').toLowerCase());
    };
    this.replaceSpaceWithUnderScore = function (cElement) {
        $(cElement).val($(cElement).val().replace(/\s+/g, '_').toLowerCase());
    };
    this.upperCaseEveryLetter = function (cElement) {
        var splitStr = $(cElement).val().toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        $(cElement).val(splitStr.join(' '));
    }
};
