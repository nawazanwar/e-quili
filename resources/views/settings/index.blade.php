<div class="row">
    <div class="col-12 mb-3 text-left">
        The button will appear below your call to action button (e.g. "Add to Cart"). with an explanatory invite to
        "Name your Price".
    </div>
    <div class="col-lg-4">
        <input type="image" title="button1.svg" src="{{asset('img/button1.svg')}}" class="form-control p-0"
               onclick="create_snippet(this);">
    </div>
    <div class="col-lg-4">
        <input type="image" title="button2.svg" src="{{asset('img/button2.svg')}}" class="form-control p-0"
               onclick="create_snippet(this);">
    </div>
    <div class="col-lg-4">
        <input type="image" title="button3.svg" src="{{asset('img/button3.svg')}}" class="form-control p-0"
               onclick="create_snippet(this);">
    </div>
    <div class="col-12 mt-3">
        <textarea class="form-control" name="equili_liquid_holder" id="equili_liquid_holder" rows="1"
                  onclick="this.focus();this.select()" readonly="readonly" style="background-color: white;"></textarea>
    </div>
    <div class="col-12 mt-3 h6">
        Copy code and add it in product.liquid after add to cart button
    </div>
</div>
