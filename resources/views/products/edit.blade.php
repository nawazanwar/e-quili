<form id="edit_product_form" action="javascript:void(0)" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-4">
            <img src="{{ $product->image }}" class="img img-thumbnail img-fluid m-auto equili_thumbnail">
            <div class="row">
                <div class="col mt-4">
                    @if(!$product->is_updated AND !$product->equili_id)
                        <div class="icheck-success d-inline">
                            <input type="checkbox" name="import_to_equili_website"
                                   id="import_to_equili_website" onclick="manage_import_equili(this);">
                            <label for="import_to_equili_website">Import to Equili website <span
                                    class="text-danger">*</span></label>
                        </div>
                    @endif
                    <hr class="my-2 @if(!$product->equili_id) force_hide @else force_show @endif"
                        id="send_to_store_holder_line">
                    <div
                        class="icheck-success d-inline @if(!$product->equili_id) force_hide @else force_show @endif"
                        id="send_to_store_holder">
                        <input type="checkbox" id="send_to_my_store" name="send_to_my_store"
                               onclick="manage_send_to_my_store(this);"
                               @if($product->equili_id AND $product->is_deleted_from_store==false ) checked @endif>
                        <label for="send_to_my_store">Send to My Store</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col mt-4">
                    <label><b>Category :<span class="text-danger">*</span> </b></label>
                    <select
                        id="category_selector"
                        class="form-control form-control-sm form-control form-control-sm-sm selectpicker show-menu-arrow"
                        data-header="Select a Category"
                        data-live-search="true"
                        @if($product->active_equili) disabled="disabled" @endif
                        name="category_id">
                        @if(count($categories)>0)
                            <option class="disabled" value="select_category">Select Categories</option>
                            @foreach($categories as $category)
                                <optgroup label="{{ $category->name }}">
                                    @php
                                        $childCategories = \App\Models\Category::childs($category->value);
                                    @endphp
                                    @if(count($childCategories)>0)
                                        @foreach($childCategories as $child_category)
                                            @php
                                                $selected = "";
                                                if ($product->category_id==$child_category->id){
                                                    $selected ='selected';
                                                }
                                            @endphp
                                            <option
                                                {{$selected}} value="{{$child_category->value}}">{{$child_category->name}}</option>
                                        @endforeach
                                    @endif
                                </optgroup>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            @include('products.components.right')
        </div>
    </div>
    <hr>
    @include('products.components.buttons')
    <div class="row my-4">
        <div class="col-12 text-right">
            <a class="btn text-white bg-gradient-info cursor-pointer btn-sm"
               onclick="submit_edit('{{$product->id}}');return false;"><i class="fa fa-spinner fa-spin"
                                                                          style="display: none;"></i> Update</a>
        </div>
    </div>
</form>
<style>
    .equili_thumbnail {
        max-height: 230px;
        width: 100%;
        object-fit: contain;
    }
</style>
<script>
    $(document).ready(function () {

        $('input[type="checkbox"]').click(function () {
            if ($(this).prop("checked") == true) {
                $(this).val(1);
            } else if ($(this).prop("checked") == false) {
                $(this).val(0);
            }
        });


    });

    function manage_import_equili(cELement) {

        if ($(cELement).is(":checked")) {

            $("#send_to_store_holder").removeClass('force_hide force_show').addClass('force_show').find('#send_to_my_store').prop('checked', true);
            $("#send_to_store_holder_line").removeClass('force_hide force_show').addClass('force_show');
            $("#active_equili_holder")
                .removeClass('force_hide force_show')
                .addClass('force_show')
                .find('#active_equili').prop('checked', true).val(1);
            $(".equili_button_holder").removeClass('force_hide force_show')
                .addClass('force_show')
                .find('.equili_btn_checkbox');
            $(".equili_btn_checkbox").removeAttr('disabled');
            $(".equili_button_holder ").find('#no_time_limit').prop("checked", true);

        } else {

            $("#send_to_store_holder").removeClass('force_hide force_show').addClass('force_hide');
            $("#send_to_store_holder").find('#send_to_my_store').prop("checked", false);
            $("#send_to_store_holder_line").removeClass('force_hide force_show').addClass('force_hide');
            $("#active_equili_holder").removeClass('force_hide force_show').addClass('force_hide');
            $("#active_equili_holder").find('#active_equili').prop("checked", false);
            $(".equili_button_holder").removeClass('force_hide force_show').addClass('force_hide');
            $(".equili_button_holder ").find('#no_time_limit').prop("checked", false);
            $(".equili_button_holder ").find('#active_on_certain_date').prop("checked", false);
            $("#from_to_date_holder").removeClass('force_hide show_inline').addClass('force_hide');
            $('#from_date').val('');
            $("#to_date").val('');
        }
    }

    function manage_send_to_my_store(cELement) {
        if ($(cELement).is(":checked")) {

            $("#active_equili_holder")
                .removeClass('force_hide force_show')
                .addClass('force_show')
                .find('#active_equili').prop('checked', true).val(1);
            $(".equili_button_holder").removeClass('force_hide force_show')
                .addClass('force_show')
                .find('.equili_btn_checkbox');
            $(".equili_btn_checkbox").removeAttr('disabled');
            $(".equili_button_holder ").find('#no_time_limit').prop("checked", true);

        } else {

            $("#active_equili_holder")
                .removeClass('force_hide force_show')
                .addClass('force_hide')
                .find('#active_equili').prop('checked', false).val(0);
            $("#active_equili_holder").find('#active_equili').prop("checked", false);
            $(".equili_button_holder").removeClass('force_hide force_show').addClass('force_hide').find('.equili_btn_checkbox').attr('disabled', 'disabled');
            $(".equili_button_holder ").find('#no_time_limit').prop("checked", false);
            $(".equili_button_holder ").find('#active_on_certain_date').prop("checked", false);
            $("#from_to_date_holder").removeClass('force_hide show_inline').addClass('force_hide');
            $('#from_date').val('');
            $("#to_date").val('');
        }
    }

    function manage_active_equili(cElement) {
        if ($(cElement).is(":checked")) {
            $('.equili_btn_checkbox').removeAttr('disabled');
            $('#no_time_limit').prop("checked", true);
        } else {
            $('.equili_btn_checkbox').attr('disabled', 'disabled');
            $("#from_to_date_holder").removeClass('force_hide show_inline').addClass('force_hide');
            $('#no_time_limit').prop("checked", false);
            $('#active_on_certain_date').prop("checked", false);
            $('#from_date').val('');
            $("#to_date").val('');
        }
    }

    function manage_equili_btn(cElement) {
        $('#from_date').val('');
        $("#to_date").val('');
        var cId = $(cElement).attr('id');
        if (cId == 'active_on_certain_date') {
            $("#from_to_date_holder").removeClass('force_hide show_inline').addClass('show_inline');
        } else {
            $("#from_to_date_holder").removeClass('force_hide show_inline').addClass('force_hide');
        }
    }

    function submit_edit(pId) {
        var brand = $("#brand").val();
        var sku = $("#sku").val();
        var selected_category = $("#category_selector").find('option:selected').val();

        var auto_offer = $('#take_bid_minimum_price_checkbox').is(':checked');
        var auto_offer_price = $("#take_bid_minimum_price").val();

        var counter_offer = $('#offer_minimum_max_checkbox').is(':checked');
        var counter_offer_minimum_price = $("#counter_offer_minimum_price").val();
        var counter_offer_maximum_price = $("#counter_offer_maximum_price").val();

        console.log(counter_offer_minimum_price, counter_offer_maximum_price, auto_offer);

        var equili_id = "{{ $product->equili_id }}";
        var send_to_my_store = $("#send_to_my_store");

        if (equili_id != '' && !send_to_my_store.is(":checked")) {
            deleteProduct(equili_id);
        } else {
            /*start */
            if (!$("#import_to_equili_website").is(":checked") && equili_id == '') {
                toastr.error('Please Checked import to Equili Button');
            } else if (brand == '' && equili_id == '') {
                toastr.error('Please Enter Brand Name');
                $("#brand").focus();
            } else if (sku == '' && equili_id == '') {
                toastr.error('Please Enter SKU');
                $("#sku").focus();
            } else if (selected_category == 'select_category') {
                toastr.error('First choose Category');
                $("#selected_category").focus();
            } else if (counter_offer_minimum_price > counter_offer_maximum_price && counter_offer) {
                toastr.error('Counter Offer Min Price should not be greater than Counter Off Max Price');
                $("#counter_offer_minimum_price").focus();
            } else if (counter_offer_maximum_price > auto_offer_price && auto_offer) {
                toastr.error('Auto-Offer accept price can\'t be lower than maximal counteroffer price');
                $("#counter_offer_maximum_price").focus();
            } else {
                var active_on_certain_date = $("#active_on_certain_date");
                if (active_on_certain_date.is(':checked')) {
                    var from_date = $("#from_date").val();
                    var to_date = $("#to_date").val();

                    if (from_date == '' || to_date == '') {

                        toastr.error('Both dates are required');
                        $("#from_date").focus();

                    } else {
                        from_date = new Date(Date.parse(from_date));
                        to_date = new Date(Date.parse(to_date));
                        var current_date = new Date();
                        if (from_date > to_date) {
                            toastr.error('From date should be less than to date');
                            $("#to_date").focus();
                        } else if (to_date < current_date) {
                            toastr.error('To date should be today or greater than today');
                            $("#to_date").focus();
                        } else {
                            call_edit_submission(pId);
                        }
                    }
                } else {
                    call_edit_submission(pId);
                }
            }
            /*end*/
        }
    }

    function call_edit_submission(pId) {
        var equili_id = "{{ $product->equili_id }}";
        if (equili_id != '') {
            update_product(equili_id);
            console.log("ready to update product");
        } else {
            sync_product(pId);
            console.log("ready to SYNC product");
        }
    }


    function sync_product(pId) {
        Ajax.setAjaxHeader();
        var formData = new FormData(document.getElementById('edit_product_form'));
        $(".fa-spinner").show();
        $.ajax({
            type: 'POST',
            url: "product/sync/" + pId,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                console.log(response.equili);
                if (response.status == 'true') {
                    var main_model = $(".main_model");
                    var modal_for = main_model.find('#modal_for').val();
                    main_model.removeClass('show hide').addClass('hide').hide();
                    toastr.success(response.product.name + " has been sync to Equili successfully");
                    $("#cRow_" + response.product.id).addClass('updated_row').empty().html(response.updated_row);
                    setTimeout(function () {
                        if ($("#cRow_" + response.product.id).hasClass('updated_row')) {
                            $("#cRow_" + response.product.id).removeClass('updated_row');
                        }
                    }, 5000);
                } else {
                    toastr.error("Some thing went wrong please try again");
                }
            }
        });
    }

    function update_product(pId) {
        $(".fa-spinner").show();
        Ajax.setAjaxHeader();
        var formData = new FormData(document.getElementById('edit_product_form'));
        $.ajax({
            type: 'POST',
            url: "product/update/" + pId,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                if (response.status == 'true') {
                    var main_model = $(".main_model");
                    main_model.removeClass('show hide').addClass('hide').hide();
                    toastr.success(response.product.name + " has been Updated Successfully");
                    $("#cRow_" + response.product.id).addClass('updated_row').empty().html(response.updated_row);
                    setTimeout(function () {
                        if ($("#cRow_" + response.product.id).hasClass('updated_row')) {
                            $("#cRow_" + response.product.id).removeClass('updated_row');
                        }
                    }, 5000);
                } else {
                    toastr.error(response.error);
                }
            }
        });
    }

    /*function for delete product*/

    function deleteProduct(pId) {
        $(".fa-spinner").show();
        $.ajax({
            type: 'GET',
            url: "product/equili/delete/" + pId,
            success: (response) => {
                console.log("deleted response" + response);
                var main_model = $(".main_model");
                main_model.removeClass('show hide').addClass('hide').hide();
                if (response.status == 'true') {
                    $(".fa-spinner").hide();
                    toastr.error(response.message);
                    $("#cRow_" + response.product.id).addClass('updated_row').empty().html(response.updated_row);
                    setTimeout(function () {
                        if ($("#cRow_" + response.product.id).hasClass('updated_row')) {
                            $("#cRow_" + response.product.id).removeClass('updated_row');
                        }
                    }, 5000);
                }
            }
        });
    }
</script>
