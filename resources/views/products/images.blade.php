<div class="row justify-content-center align-items-center">
    @if(count($images))
        @foreach($images as $image)
            <div class="col-md-3 my-2">
                <img src="{{ $image->src }}" class="img-fluid img-thumbnail">
            </div>
        @endforeach
    @else
        <div class="col-12 text-center py-2">
            <p>No images Found</p>
        </div>
    @endif
</div>
