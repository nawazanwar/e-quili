@php
    $equili_status = $product->equili_status;
    if($equili_status=='Pending'){
        $response = \App\Models\Equili::check_product_status($product->equili_id);
        if (isset($response['status'])) {
            $product->equili_status = $response['status'];
            $product->save();
        }
        $equili_status = $product->equili_status;
    }
@endphp
<td class="text-center" style="width: 10px;" id="my_store_product_holder_{{$product->shop_id}}_{{$product->id}}">
    <div class="icheck-primary d-inline">
        <input type="checkbox" id="check-{{$product->id}}" @if($product->equili_status=='Approved' AND $product->is_deleted_from_store=='0') checked
               @endif onclick="return false;">
        <label for="check-{{$product->id}}"></label>
    </div>
</td>
<td class="text-left">
    <div class="image_holder">
        <img src="{{ $product->image }}" class="img-fluid img-thumbnail" style="width: 50px;">
        <div class="overlay"></div>
        <a class="btn btn-xs" data-toggle="tooltip" title="View All images"
           onclick="getProductImages('{{$product->id}}','{{$product->name}}');">
            <i class="fa fa-eye-slash"></i>
        </a>
    </div>
</td>
<td style="max-width:130px;overflow: hidden;">
    <p class="text-muted remove-spaces my-0 font-weight-bold">{{ $product->name }}</p>
    <p class="my-0">
        <span class="text-info">SKU :</span>
        <span class="text-muted small">{{ ($product->sku)?$product->sku:'No Sku' }}</span>
    </p>
</td>
<td>
    <p class="text-muted remove-spaces my-0 font-weight-bold">
        {{ $product->getCategory() }}
    </p>
    <p class="my-0">
        <small>Price : {{$product->estimated_market_price}}</small>
    </p>
</td>
<td class="text-center" id="status_holder_{{$product->shop_id}}_{{$product->id}}">
    @if($product->equili_id)
        @php
            $status_class="";
            if ($equili_status=='Pending'){
                $status_class = "badge-warning";
            }else if ($equili_status=='Approved'){
                 $status_class = "badge-success";
            }
        @endphp
        <p class="badge font-weight-normal my-0 {{$status_class}}"
           style="font-size: 11px;padding: 5px;">{{ $equili_status }}</p>
    @else
        --
    @endif
</td>
<td class="text-center" id="button_status_holder_{{$product->shop_id}}_{{$product->id}}">
    @if($product->active_equili  AND $product->is_deleted_from_store=='0')
        @if($product->equili_limit=='no_time_limit')
            @if($product->equili_status=='Approved')
                <p class="badge font-weight-normal my-0 badge-success no_limit_status_btn" style="font-size: 11px;padding: 5px;">
                    Active
                </p>
            @else
                <p class="badge font-weight-normal my-0 badge-danger no_limit_status_btn" style="font-size: 11px;padding: 5px;">
                    In Active
                </p>
            @endif
        @else
            @isset($product->equili_from)
                <p class="my-0">{{ $product->equili_from }}</p>
            @endisset
            @isset($product->equili_to)
                <p class="my-0">{{ $product->equili_to }}</p>
            @endisset
        @endif
    @else
        <p class="badge font-weight-normal my-0 badge-danger" style="font-size: 11px;padding: 5px;">
            In Active
        </p>
    @endif
</td>
<td>{{ $product->created_at }}</td>
<td class="text-center">
    <a class="btn btn-info btn-xs text-white cursor-pointer"
       data-toggle="tooltip" data-placement="top" title="Edit {{$product->name}}"
       onclick="editProduct('{{$product->id}}','{{$product->name}}');">
        <i class="fa fa-edit"></i>
    </a>
</td>
