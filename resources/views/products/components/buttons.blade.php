<div class="row justify-content-center">
    <div class="col-md-4">
        <div
            class="icheck-success d-inline @if($product->is_deleted_from_store OR !$product->equili_id) force_hide @else force_show @endif"
            id="active_equili_holder">
            <input type="checkbox" name="active_equili" id="active_equili"
                   onclick="manage_active_equili(this);" value="1"
                   @if($product->active_equili AND $product->is_deleted_from_store==false)  checked @endif>
            <label for="active_equili">Active Equili button</label>
        </div>
    </div>
    <div class="col-md-4">
        <div
            class="icheck-success d-inline @if($product->is_deleted_from_store==true) force_hide @else force_show @endif equili_button_holder">
            <input type="radio"
                   @if($product->active_equili AND $product->equili_limit =='no_time_limit') checked @endif
                   class="equili_btn_checkbox"
                   id="no_time_limit"
                   value="no_time_limit"
                   onchange="manage_equili_btn(this)"
                   name="equili_limit" {{--@if(!$product->active_equili) disabled="disabled" @endif--}}>
            <label for="no_time_limit">No limited time</label>
        </div>
    </div>
    <div class="col-md-4">
        <div
            class="icheck-success d-inline @if($product->is_deleted_from_store==true) force_hide @else force_show @endif equili_button_holder">
            <input type="radio"
                   @if($product->active_equili AND $product->equili_limit =='active_on_certain_date') checked @endif
                   class="equili_btn_checkbox"
                   id="active_on_certain_date"
                   value="active_on_certain_date"
                   onchange="manage_equili_btn(this)"
                   name="equili_limit" {{--@if(!$product->active_equili) disabled="disabled" @endif--}}>
            <label for="active_on_certain_date">Activate on certain date</label>
        </div>
    </div>
</div>
<div
    class="row mt-2 @if(($product->active_equili AND $product->equili_limit =='active_on_certain_date') AND  $product->is_deleted_from_store==false) show_inline @else force_hide @endif justify-content-center"
    id="from_to_date_holder">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-primary btn-sm" type="button">From</button>
            </div>
            <input
                type="date"
                class="form-control form-control-sm datemask"
                id="from_date"
                name="equili_from"
                value="{{ $product->equili_from }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <button class="btn btn-primary btn-sm" type="button">To</button>
            </div>
            <input
                type="date"
                class="form-control form-control-sm datemask"
                id="to_date"
                name="equili_to"
                value="{{ $product->equili_to }}">
        </div>
    </div>
</div>
