<div class="row">
    <!-- inputs in Model -->
    <div class="col">
        <label><b>Brand : <span class="text-danger">*</span></b></label>
        <input type="text" name="brand" id="brand" placeholder="Enter brands" value="{{ $product->brand }}"
               class="form-control form-control-sm"
               @if($product->active_equili) disabled="disabled" @endif>
    </div>
    <div class="col">
        <label><b>Model :</b></label>
        <input type="text" name="product_model" id="product_model" placeholder="Enter Product model"
               class="form-control form-control-sm" value="{{$product->product_model}}"
               @if($product->active_equili) disabled="disabled" @endif>
    </div>
    <div class="col">
        <label><b>EAN :</b></label>
        <input type="text" name="ean" id="ean" placeholder="Enter EAN" class="form-control form-control-sm"
               value="{{ $product->ean }}"
               @if($product->active_equili) disabled="disabled" @endif>
    </div>
    <div class="col">
        <label><b>SKU :<span class="text-danger">*</span></b></label>
        <input type="text" name="sku" id="sku" value="{{ $product->sku }}"
               class="form-control form-control-sm"
               @if($product->active_equili) disabled="disabled" @endif>
    </div>
</div>
<div class="row ml-2 mt-3">
    <div class="col-6 custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="take_bid_minimum_price_checkbox"
               name="take_bid_minimum_price_checkbox" @if($product->take_bid_minimum_price) checked @endif
               onchange="changeSwitcher(this,'take_bid_minimum_price');">
        <label class="custom-control-label" for="take_bid_minimum_price_checkbox">Auto-Offer price accept as of:</label>
    </div>
    <div class="col-3 pl-1">
        <input type="number" id="take_bid_minimum_price" class="form-control form-control-sm"
               name="take_bid_minimum_price" value="{{ $product->take_bid_minimum_price }}"
               @if(!$product->take_bid_minimum_price) disabled="" @endif>
    </div>
</div>
{{--alert_minimum_price_code--}}
<div class="row ml-2 mt-3">
    <div class="col-6 custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="alert_minimum_price_checkbox"
               name="alert_minimum_price_checkbox" @if($product->alert_minimum_price) checked
               @endif onchange="changeSwitcher(this,'alert_minimum_price');">
        <label class="custom-control-label" for="alert_minimum_price_checkbox">Get offer notifications as of:</label>
    </div>
    <div class="col-3 pl-1">
        <input type="number" class="form-control form-control-sm" name="alert_minimum_price"
               value="{{ $product->alert_minimum_price }}"
               id="alert_minimum_price" @if(!$product->alert_minimum_price) disabled="" @endif>
    </div>
</div>
{{--counter_offer_minimum__max_price--}}
<div class="row ml-2 mt-3">
    <div class="col-6 custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="offer_minimum_max_checkbox"
               name="offer_minimum_max_checkbox"
               @if($product->counter_offer_minimum_price OR  $product->counter_offer_maximum_price ) checked
               @endif
               onchange="minMaxSwitcher(this,'counter_offer_minimum_price','counter_offer_maximum_price')">
        <label class="custom-control-label" for="offer_minimum_max_checkbox">For Offers in the following range:</label>
    </div>
    <div class="col-6 pl-1">
        <div class="input-group mb-3">
            <label for="counter_offer_minimum_price"></label>
            <input type="text"
                   class="form-control form-control-sm"
                   placeholder="Min price"
                   name="counter_offer_minimum_price"
                   id="counter_offer_minimum_price"
                   value="{{ $product->counter_offer_minimum_price }}"
                   @if(!$product->counter_offer_minimum_price OR  !$product->counter_offer_maximum_price) disabled @endif>
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
            </div>
            <input type="text"
                   class="form-control form-control-sm"
                   placeholder="Max price"
                   name="counter_offer_maximum_price"
                   id="counter_offer_maximum_price"
                   value="{{ $product->counter_offer_maximum_price }}"
                   @if(!$product->counter_offer_minimum_price OR  !$product->counter_offer_maximum_price) disabled @endif>
        </div>
    </div>
</div>
{{--counter_offer_price--}}
<div class="row ml-2 mt-3">
    <label class="col-6" for="counter_offer_price"> Send Counter-offer of:</label>
    <div class="col-3 pl-1">
        <input type="number"
               class="form-control form-control-sm"
               name="counter_offer_price"
               value="{{ $product->counter_offer_price }}"
               id="counter_offer_price">
    </div>
</div>
<div class="row mb-1">
    <!--<div class="col-12">-->
    <!--    <label for="short_description"><b>Short Description-->
    <!--            :</b></label>-->
    <!--    <textarea class="form-control form-control-sm" rows="3" id="short_description"-->
    <!--              name="short_description">{{ $product->short_description }}</textarea>-->
    <!--</div>-->
    <div class="col-12">
        <label for="description"><b>Product description
                :</b></label>
        <textarea class="form-control form-control-sm" id="description" rows="3"
                  name="description">{{ $product->description }}</textarea>
    </div>
</div>
