<div class="table-responsive">
    <table class="table">
        <thead class="bg-primary text-white">
        <tr>
            <th class="text-center" style="width: 10px;">
                <a data-toggle="tooltip" title="My store products"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
            </th>
            <th colspan="2">Name</th>
            <th>Category / price</th>
            <th class="text-center">Equili Status</th>
            <th class="text-center">Equili Button Status</th>
            <th>Date</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="product_container">

        </tbody>
    </table>
</div>
