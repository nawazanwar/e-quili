<div class="row justify-content-center align-items-center mx-auto my-3" id="filter_holder" style="display: none;">
    <div class="col-md-2 form-group mb-md-0">
        <input type="text" name="name" class="form-control form-control-sm" placeholder="Product name"
               onkeyup="applyFilter(this)">
    </div>
    <div class="col-md-2 form-group mb-md-0">
        <input type="text" name="sku" class="form-control form-control-sm" placeholder="SKU"
               onkeyup="applyFilter(this)">
    </div>
    <div class="col-md-2 form-group mb-md-0">
        @php
            $categories =\App\Models\Category::all();
        @endphp
        <select
            class="form-control form-control-sm form-control form-control-sm-sm selectpicker show-menu-arrow"
            data-size="5"
            data-selected-text-format='count > 5'
            data-header="Select a Category"
            data-live-search="true"
            onchange="applyFilter(this)"
            name="category_id">
            @if(count($categories)>0)
                <option class="disabled">Select Categories</option>
                @foreach($categories as $category)
                    <optgroup label="{{ $category->name }}">
                        @php
                            $childCategories = \App\Models\Category::childs($category->value);
                        @endphp
                        @if(count($childCategories)>0)
                            @foreach($childCategories as $child_category)
                                <option value="{{$child_category->id}}">{{$child_category->name}}</option>
                            @endforeach
                        @endif
                    </optgroup>
                @endforeach
                <option value="all">All Categories</option>
            @endif
        </select>
    </div>
    <div class="col-md-2 form-group mb-md-0">
        <select name="equili_status" class="form-control form-control-sm" onchange="applyFilter(this)">
            <option class="disabled">Select Status</option>
            <option value="Pending">Pending</option>
            <option value="Approved">Approved</option>
            <option value="not-approved"> Not Approved</option>
            <option value="all">All Status</option>
        </select>
    </div>
</div>
