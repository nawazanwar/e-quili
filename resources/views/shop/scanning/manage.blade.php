<div class="row">
    <div class="col-12 my-1">
        <p class="total_scanned pt-0">Total <strong class="text-success scanned_percentage">{{$scanned_percentage}}%</strong> scanned</p>
        <div class="progress-group">
            <div class="progress progress-xl">
                <div class="progress-bar bg-gradient-success" id="progress_bar"
                     style="width: {{$scanned_percentage}}%"></div>
            </div>
        </div>
    </div>
    <div class="col-12 my-1 text-right">
        <button class="btn bg-gradient-success btn-md text-white cursor-pointer" id="load_shopify_products"
                data-id="{{$last_product}}" onclick="startScanning(this);">
            Scan more products
        </button>
    </div>
    <div class="col-12 my-1 text-center py-2" style="display: none;" id="wait_scanning">
        <img src="{{ asset('img/loader.gif') }}" style="width:100px;">
    </div>
</div>
