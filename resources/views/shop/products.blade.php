<div class="card card-secondary">
    <h3 class="text-center p-3 m-0 font-weight-bold">Total <strong class="text-success"
                                                                   id="pulled_products"></strong> products has
        Pulled </h3>
    <div class="mx-3">
        <div class="progress-group">
            Total <strong class="font-weight-bold" id="shop_products"></strong> exists in Store
            <span class="float-right" id="progress_bar_text">0% Completed</span>
            <div class="progress progress-xl">
                <div class="progress-bar bg-gradient-success" id="progress_bar" style="width: 0%"></div>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body py-4 text-right">
        <button class="btn bg-gradient-success btn-sm text-white cursor-pointer" id="load_shopify_products" data-id="0" onclick="loadShopifyProducts(this);">
            Get More products
        </button>
    </div>
</div>
