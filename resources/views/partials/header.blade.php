<nav class="main-header navbar navbar-expand navbar-white navbar-light ml-0 py-1">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item d-none d-sm-inline-block">
            <a class="nav-link">
                <img src="{{ asset('img/logo.png') }}"
                     style="width: 63px;">
            </a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link">
                Welcome Back <span class="text-info">{{ $shop->seller_name }}</span>
            </a>
        </li>
        <li class="nav-item">
            <button class="nav-link cursor-pointer btn btn-outline-primary mx-1"
                    style="height: 30px;width: 30px;padding:0;margin: 5px;"
                    onclick="open_setting_modal();">
                <span class="fa fa-cog"></span>
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link cursor-pointer btn btn-outline-warning mx-1"
                    style="height: 30px;width: 30px;padding:0;margin: 5px;" onclick="logOut();">
                <span class="fa fa-power-off"></span>
            </button>
        </li>
    </ul>
</nav>
