<style>
    .loader {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1000;
        display: none;
        background-color: transparent;
    }

    .loader img {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<div class="loader">
    <img src="{{ asset('img/loader.gif') }}" style="width:150px;">
</div>
