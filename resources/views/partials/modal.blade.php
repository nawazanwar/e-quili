<div class="modal fade main_model hide" id="modal-lg" aria-hidden="true" style="background-color: rgba(0,0,0,.5);">
    <input type="hidden" value="" id="modal_for">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header py-2">
                <h4 class="modal-title"></h4>
                <button type="button" class="close close_model" data-dismiss="modal" aria-label="Close"
                        onclick="closeModel();">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
