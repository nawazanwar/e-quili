@extends('layouts.app')
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <section class="content-header pb-0">
        <div class="container-fluid">
            <div class="row mb-2 align-items-center">
                <div class="col-sm-6">
                    <h4 class="my-0">All Products</h4>
                    <p class="mt-0 mb-1 text-sm">Total products <span class="text-info total_products"></span> / Scanned
                        <span
                            class="text-success scanned_products"></span></p>
                </div>
                <div class="col-sm-6 text-right">
                    <a class="btn btn-sm btn-outline-info mx-1 cursor-pointer" onclick="toggleFiltering();">Filter</a>
                    {{--<a class="btn btn-sm btn-outline-info mx-1 cursor-pointer" onclick="manageEquili();">Scan Equili</a>--}}
                    <a class="btn btn-sm btn-outline-info mx-1 cursor-pointer" onclick="loadScanning();">Scan
                        Shopify</a>
                </div>
            </div>
            @include('products.filter')
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">
                @include('products.index')
            </div>
        </div>
    </section>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/exteral.js') }}"></script>
@endsection
@section('pageScript')
    <script>
        ShopifyApp.init({
            apiKey: '{{ config('system.api_key') }}',
            shopOrigin: 'https://{{ $shop->name }}'
        });
        ShopifyApp.ready(function () {
            $(".loader").show();
            $('[data-toggle="tooltip"]').tooltip();
            getShopProducts();
            $("#home_tab").click(function () {
                $("#product_container").empty();
                $(".loader").show();
                getShopProducts();
            });

            $("#pull_tab").click(function () {
                var last_sku = "{{ \App\Models\Product::getLastSku($shop->id) }}";
                $("#load_more_shopify_products").attr('data-since-id', last_sku);
            });

            $('input[type="checkbox"]').click(function () {
                if ($(this).prop("checked") == true) {
                    $(this).val(1);
                } else if ($(this).prop("checked") == false) {
                    $(this).val(0);
                }
            });

            window.setInterval(function () {

                Ajax.setAjaxHeader();
                Ajax.call('update/status/{{ $shop->id }}', null, 'GET', function (response) {
                    if (response.status == 'true') {
                        var products = response.products;
                        $.each(products, function (key, value) {
                            if (value == 'Approved') {
                                var status_id = "status_holder_{{$shop->id}}_" + key;
                                var button_status_holder = "button_status_holder_{{$shop->id}}_" + key;
                                var my_store_product_holder = "my_store_product_holder_{{$shop->id}}_" + key;
                                $("#" + status_id).empty().html('<p class="badge font-weight-normal my-0 badge-success" style="font-size: 11px;padding: 5px;">Approved</p>');
                                $("#" + button_status_holder).find('.no_limit_status_btn').empty().text('Active').removeClass('badge-danger badge-success').addClass('badge-success');
                                $("#" + my_store_product_holder).find('input').prop('checked', true);
                            }
                        })
                    }
                });

            }, 20000);

        });

        function changeSwitcher(cElement, cInput) {
            if ($(cElement).is(':checked')) {
                $("#" + cInput).removeAttr('disabled');
            } else {
                $("#" + cInput).attr('disabled', 'disabled');
            }
        }

        function minMaxSwitcher(cElement, minInput, maxInput) {
            if ($(cElement).is(':checked')) {
                $("#" + minInput).removeAttr('disabled');
                $("#" + maxInput).removeAttr('disabled');
            } else {
                $("#" + minInput).attr('disabled', 'disabled');
                $("#" + maxInput).attr('disabled', 'disabled');
            }
        }

        var filter_name, filter_equili_status, filter_sku, filter_category_id;

        function applyFilter(cElement) {
            var key = $(cElement).attr('name');
            if (key == 'name') {
                filter_name = $(cElement).val();
            }
            if (key == 'category_id') {
                filter_category_id = $(cElement).val();
            }
            if (key == 'sku') {
                filter_sku = $(cElement).val();
            }
            if (key == 'equili_status') {
                filter_equili_status = $(cElement).val();
            }

            var object = {
                'filter_name': filter_name,
                'filter_sku': filter_sku,
                'filter_equili_status': filter_equili_status,
                'filter_category_id': filter_category_id
            }
            if (filter_equili_status == 'all') {
                delete object['filter_equili_status'];
            }
            if (filter_category_id == 'all') {
                delete object['filter_category_id'];
            }
            getShopProducts(object);
        }

        function getShopProducts(params) {
            var route = 'products?shop_id={{$shop->id}}';
            if (typeof params != "undefined") {
                route = route + "&filter_name=" + params.filter_name + "&filter_sku=" + params.filter_sku + "&filter_equili_status=" + params.filter_equili_status + "&filter_category_id=" + params.filter_category_id;
            }
            Ajax.call(route, null, 'GET',
                function (response) {
                    if (response.status == "true") {
                        $(".loader").fadeOut("slow");
                        $("#product_container").empty().html(response.html);
                        $(".total_products").empty().text(response.total_products);
                        $(".scanned_products").empty().text(response.scanned_products);
                        $(".scanned_percentage").empty().html(response.scanned_percentage + "% scanned");
                        $("#progress_bar").removeAttr('style').css({
                            "width": response.scanned_percentage + "%"
                        });
                        $("#load_shopify_products").attr('data-id', parseInt(response.last_product));
                    }
                });
        }

        function logOut() {
            if (confirm('Loging out your seller account from Equili will deactivate all of your "Make an Offer" buttons at your Online Shop for as long as you\'re logged out')) {
                $(".loader").show();
                Ajax.call('{{ route('user.logout',[$shop->id]) }}', null, 'GET', function (response) {
                    if (response.status == 'true') {
                        toastr.success("SuccessFully Logout Your Seller Account");
                        $.each(response.deleted_rows, function (key, value) {
                            var id = key.split("updated_row_")[1];
                            $("#cRow_" + id).addClass('updated_row').empty().html(value);
                            setTimeout(function () {
                                if ($("#cRow_" + id).hasClass('updated_row')) {
                                    $("#cRow_" + id).removeClass('updated_row');
                                }
                            }, 5000);
                        });
                        $(".loader").hide();
                        location.reload();
                    }
                })
            }
            return false;
        }

        function getProductImages(pId, pTitle) {

            var main_model = $(".main_model");
            main_model.find('#modal_for').val('product_images');
            main_model.removeClass('show hide').addClass('show').show();
            main_model.find('.modal-title').empty().html('list of all images of ' + pTitle);
            main_model.find('.modal-body').addClass('text-center').empty().html('<img src="{{ asset('img/loader.gif') }}" class="py-2" style="width:100px;">');

            Ajax.call('product/images?pId=' + pId + "&shop={{$shop->id}}", null, 'GET', function (response) {
                main_model.find('.modal-body').empty().html(response);
            })

        }

        function closeModel() {
            var main_model = $(".main_model");
            var modal_for = main_model.find('#modal_for').val();
            if (modal_for == 'load_scanning') {
                $('.modal').show();
                getShopProducts();
            }
            main_model.removeClass('show hide').addClass('hide').hide();
        }

        function editProduct(pId, pTitle) {
            $("body").css('overflow', 'hidden !important');
            var main_model = $(".main_model");
            main_model.find('#modal_for').val('edit_product');
            main_model.removeClass('show hide').addClass('show').show();
            main_model.find('.modal-title').empty().html(pTitle);
            main_model.find('.modal-body').addClass('text-center').empty().html('<img src="{{ asset('img/loader.gif') }}" class="py-2" style="width:100px;">');
            Ajax.call("product/edit/" + pId, null, 'GET', function (response) {
                main_model.find('.modal-body').removeClass('text-center').empty().html(response);
                $("#category_selector").selectpicker('refresh');
            })
        }

        function loadCategories() {
            console.log("ready to load categories");
        }

        function loadScanning() {
            var main_model = $(".main_model");
            main_model.find('#modal_for').val('load_scanning');
            main_model.removeClass('show hide').addClass('show').show();
            main_model.find('.modal-title').empty().html('Scanned Products from Shopify Store');
            main_model.find('.modal-body').addClass('text-center').empty().html('<img src="{{ asset('img/loader.gif') }}" class="py-2" style="width:100px;">');
            Ajax.call("shop/scan/manage?shop_id={{$shop->id}}", null, 'GET', function (response) {
                main_model.find('.modal-body').empty().html(response);
            })
        }

        function startScanning(cElement) {
            var since_id = $(cElement).attr('data-id');
            $("#wait_scanning").show();
            Ajax.call("shop/scan/start?shop_id={{$shop->id}}&since_id=" + since_id, null, 'GET', function (response) {
                if (response.status == "true") {
                    $("#wait_scanning").hide();
                    $(".total_products").empty().text(response.total_products);
                    $(".scanned_products").empty().text(response.scanned_products);
                    $(".scanned_percentage").empty().html(response.scanned_percentage + "% scanned");
                    $("#progress_bar").removeAttr('style').css({
                        "width": response.scanned_percentage + "%"
                    });
                    $("#load_shopify_products").attr('data-id', parseInt(response.last_product));
                }
            });
        }

        function toggleFiltering() {

            $("#filter_holder").slideToggle('slow');

        }

        function startFiltering() {

            var name = $("#product_name").val();
            var sku = $("#product_sku").val();
            var brand = $("#product_brand").val();
            var status = $("#product_status").val();
            var data = {
                'name': name,
                'sku': sku,
                'brand': brand,
                'status': status
            }
            $(".loader").show();
            getShopProducts(data)
        }

        function showHideDateButtons(cElement) {
            if ($(cElement).is(':checked')) {
                $(".equili_btn_checkbox").removeAttr('disabled');
            } else {
                $(".equili_btn_checkbox").attr('disabled', 'disabled');
            }
        }

        function hideFromToDate() {
            $("#from_date").attr('disabled', 'disabled');
            $("#to_date").attr('disabled', 'disabled');
        }

        function showFromToDate() {
            $("#from_date").removeAttr('disabled');
            $("#to_date").removeAttr('disabled');
        }

        function changeImportEquili(cElement) {
            if ($(cElement).is(':checked')) {
                $(".import_holder").show();
            } else {
                $(".import_holder").hide();
            }
        }

        function open_setting_modal() {

            var main_model = $(".main_model");
            main_model.find('#modal_for').val('load_scanning');
            main_model.removeClass('show hide').addClass('show').show();
            main_model.find('.modal-title').empty().html('Choose a Button Style');
            main_model.find('.modal-body').addClass('text-center').empty().html('<img src="{{ asset('img/loader.gif') }}" class="py-2" style="width:100px;">');
            Ajax.call("settings", null, 'GET', function (response) {
                main_model.find('.modal-body').empty().html(response);
            })

        }

        function create_snippet(cElement) {
            var shop_id = "{{ $shop->id }}";
            var src = $(cElement).attr('title');
            Ajax.call("settings/add-snippet", {
                'shop_id': shop_id,
                'src': src
            }, 'POST', function (response) {
                if (response.status == 'true') {
                    $("#equili_liquid_holder").removeAttr('disabled').html("{% include 'equili' %}").select();
                }
            });
        }
    </script>
@stop
