@extends('layouts.auth')
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection
@section('content')
    <div class="card">
        <div class="card-body login-card-body" style="border-radius: 10px;">
            <div class="form-group text-center my-3">
                <img src="{{ asset('img/logo.png') }}" style="width: 113px;">
            </div>
            <div class="input-group mb-3">
                <input id="email" type="email" class="form-control b-n"
                       name="email" required autocomplete="off"
                       autofocus
                       placeholder="{{ __('auth.enter_email') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope text-gray-dark"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input id="password" type="password" class="form-control b-n"
                       autocomplete="off" name="password" required autocomplete="current-password"
                       placeholder="{{ __('auth.enter_password')}}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock text-gray-dark"></span>
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-primary btn-block" id="login_btn">{{__('auth.login')}}</button>
            </div>
            <div class="form-group text-center">
                <a href="https://e-quili.com/become-a-seller" target="_blank">Create Account</a> OR <a
                    href="https://e-quili.com/forgot-password" target="_blank">Reset Password</a>
            </div>
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/exteral.js') }}"></script>
@endsection
@section('pageScript')
    <script>
        ShopifyApp.init({
            apiKey: '{{ config('system.api_key') }}',
            shopOrigin: 'https://{{ $shop->name }}'
        });
        var email, password;
        ShopifyApp.ready(function () {
            $("#login_btn").click(function () {
                email = $("#email").val();
                password = $("#password").val();
                Ajax.call('{{ route("user.login",[$shop->id]) }}', {
                    'email': email,
                    'password': password
                }, 'POST', function (response) {
                    if (response.status == 'validate_errors') {
                        for (var property in response.errors) {
                            toastr.error(response.errors[property][0])
                        }
                    } else if (response.status == 'error') {
                        toastr.error(response.message)
                    } else {
                        $(".loader").show();
                        location.reload();
                    }
                })
            })
        });
    </script>
@endsection
