<div class="card card-default">
    <div class="card-header">
        <div class="card-tools">
            <button class="btn btn-sm btn-outline-info mx-1 cursor-pointer"
                    onclick="scanEquiliCategories('eCategoryHolder');">Start scanning
            </button>
        </div>
    </div>
    <div class="card-body text-left" id="eCategoryHolder">
        @if(count($categories)>0)
            @foreach($categories as $key=>$category)
                <div class="card direct-chat direct-chat-primary collapsed-card">
                    <div class="card-header ui-sortable-handle" style="cursor: move;">
                        <h3 class="card-title">{{ $category->name }}</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="display: none;">
                        @php
                            $childCategories = \App\Models\Category::childs($category->value);
                        @endphp
                        @if(count($childCategories)>0)
                            <ul class="list-group-flush">
                                @foreach($childCategories as $child_category)
                                    <li class="list-group-item">{{ $child_category->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
