<div class="tab-content">
    <div class="tab-pane fade show active" id="tab-home" role="tabpanel">
        @include('products.index')
    </div>
    <div class="tab-pane fade" id="tab-pulled" role="tabpanel">
        @include('shop.products')
    </div>
    <div class="tab-pane fade" id="tab-ecategory" role="tabpanel">
        @include('equili.categories')
    </div>
</div>
