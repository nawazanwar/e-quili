<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="pill" href="#tab-home" role="tab" id="home_tab"
           aria-selected="true">{{__('system.home')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="pill" href="#tab-pulled" role="tab" id="pull_tab"
           aria-selected="true">{{__('system.sync')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="pill" href="#tab-ecategory" role="tab" id="ecategory_tab"
           onclick="loadCategories();"
           aria-selected="true">Categories</a>
    </li>
</ul>
