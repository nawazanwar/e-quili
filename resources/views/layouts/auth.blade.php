<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    @stack('styles')
    @yield('styleInnerFiles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="login-page"
      style="background-image: url('{{ asset('img/auth_bg.jpg') }}');background-repeat: no-repeat;background-size: cover;">

<div class="overlay opacity-95"></div>
<div class="login-box">
    @yield('content')
</div>
<script src="{{ asset('js/vendor.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
@include('partials.loader')
</body>
</html>
