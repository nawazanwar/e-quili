<?php

return [
    'all_products' => 'List of all Products',
    'no_image_found' => 'No Image Found',
    'sync' => 'Synchronization',
    'home' => 'Home',
    'pulled' => 'Pulled',
    'load_more' => 'Load More',
    'search_here' => 'Search Here',
];
