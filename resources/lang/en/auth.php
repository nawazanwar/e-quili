<?php

return [
    'logout' => 'Log Out',
    'login_heading' => 'Sign in to start your session',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'enter_password' => 'Enter Password',
    'enter_email' => 'Enter Email address',
    'login_title' => 'Login to View Dashboard',
    'login' => 'Login',
];
