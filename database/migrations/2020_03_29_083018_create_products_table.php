<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {
            return true;
        }

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->text('product_id')->nullable();
            $table->text('sku')->nullable();
            $table->longText('name')->nullable();
            $table->string('estimated_market_price')->nullable();
            $table->string('take_bid_minimum_price')->nullable();
            $table->string('counter_offer_minimum_price')->nullable();
            $table->string('counter_offer_maximum_price')->nullable();
            $table->string('alert_minimum_price')->nullable();
            $table->string('counter_offer_price')->nullable();
            $table->text('category')->nullable();
            $table->string('product_match')->nullable();
            $table->string('product_model')->nullable();
            $table->string('brand')->nullable();
            $table->string('ean')->nullable();
            $table->string('image')->nullable();
            $table->longText('description')->nullable();
            $table->longText('short_description')->nullable();
            $table->boolean('active_equili')->default(false);
            $table->string('equili_limit')->nullable();
            $table->string('equili_from')->nullable();
            $table->string('equili_to')->nullable();
            $table->string('equili_status')->nullable();
            $table->longText('equili_id')->nullable();
            $table->boolean('is_updated')->default(false);
            $table->boolean('is_deleted_from_store')->default(false);
            $table->timestamps();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
