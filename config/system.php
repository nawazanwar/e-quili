<?php
return [
    'absolute_url' => 'https://logiceverest.com/equili/public/',
    'equili_script' => 'https://logiceverest.com/equili/public/js/equili.js',
    'app_name' => 'e-quili',
    'api_version' => '2020-01',
    'redirect_uri' => 'https://logiceverest.com/equili/public/generate_token',
    'api_key' => 'd5bac18590ad6dbf8f1b320e526715f9',
    'secret_key' => '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',
    'scopes' => 'read_content,write_content,read_themes,write_themes,read_script_tags,write_script_tags,read_products,write_products,read_product_listings,read_inventory,write_inventory,write_checkouts'
];
//,read_reports,write_reports,read_price_rules,write_price_rules,read_discounts,write_discounts,read_shopify_payments_payouts
