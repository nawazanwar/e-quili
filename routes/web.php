<?php

Route::get('', ['as' => 'home', 'uses' => 'HomeController@index'])->middleware('xFrame');
Route::get('/install', ['as' => 'app.install', 'uses' => 'HomeController@install']);
Route::get('/generate_token', ['as' => 'app.generate_token', 'uses' => 'HomeController@generate_token']);

Route::get('products', ['as' => 'products', 'uses' => 'ProductController@index']);
Route::get('product/images', ['as' => 'product.images', 'uses' => 'ProductController@images']);
Route::get('product/edit/{pId}', ['as' => 'product.edit', 'uses' => 'ProductController@edit']);
Route::post('product/update/{pId}', ['as' => 'product.update', 'uses' => 'ProductController@update']);
Route::post('product/sync/{pId}', ['as' => 'product.sync', 'uses' => 'ProductController@sync']);
Route::get('product/equili/delete/{pId}', ['as' => 'product.equili.delete', 'uses' => 'EquiliController@delete_product']);

Route::get('product/show/{id}',
    ['as' => 'product.show', 'uses' => 'ProductController@show']
)->middleware("cors");

Route::get('product/check-eq-status/{id}',
    ['as' => 'product.check-eq-status', 'uses' => 'ProductController@check_eq_status']
)->middleware("cors");

Route::post('user/login/{pId}', ['as' => 'user.login', 'uses' => 'EquiliController@login']);
Route::get('user/logout/{pId}', ['as' => 'user.logout', 'uses' => 'EquiliController@logout']);

Route::get('shop/scan/manage', ['as' => 'shop.scan.manage', 'uses' => 'ShopController@manage']);
Route::get('shop/scan/start', ['as' => 'shop.scan.start', 'uses' => 'ShopController@scan_products']);

Route::get('equili/manage', ['as' => 'equili.manage', 'uses' => 'EquiliController@manage']);

/*check product status*/

Route::get('products/status', ['as' => 'products.status', 'uses' => 'EquiliController@check_product_status']);

Route::get('settings', ['as' => 'settings', 'uses' => 'DashboardController@settings']);

Route::post('settings/add-snippet', ['as' => 'settings.add-snippet', 'uses' => 'DashboardController@add_snippet']);

Route::get('update/status/{shop}', ['as' => 'update.status', 'uses' => 'EquiliController@update_product_status']);
